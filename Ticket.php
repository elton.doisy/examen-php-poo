<?php 

class Ticket
{
    private String $reference;
    private float $price;
    private float $taxe = 2500;

    public function __construct( String $reference, float $price)
    {
        $this->reference = $reference;
        $this->price = $price;
    }   

    private function label()
    {
        return (string)$this->price;
    }
    
    private function cost()
    {
        return (int)$this->price * 100 ;
    }

}