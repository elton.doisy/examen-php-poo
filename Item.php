<?php 

class Item
{
    protected String $name;
    protected float $price;
    protected int $weight;

    public function __construct(String $name, float $price, int $weight)
    {
        $this->name = $name ;
        $this->price = $price;
        $this->weight = $weight;
    }   

    public function getPrice()
    {
        return $this->price;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function getName()
    {
        return $this->name;
    }

    public function info()
    {
        $price =  number_format($this->price, 2, '.', ' ');
        return $this->getName().': '.$price.' € :'. $this->getWeight().'g';
    }
}