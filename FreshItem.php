<?php 

class FreshItem extends Item
{
    protected String $bestBeforeDate;
    protected String $format = 'Y-m-d';

    public function __construct(String $name, float $price, int $weight ,String $date,)
    {
        parent::__construct($name, $price, $weight);
        $d = DateTime::createFromFormat($this->format, $date);
        if (!($d && $d->format($this->format) == $date)) { // date verification,if the date is incorrect the product will expire 
            $this->bestBeforeDate = '2022-01-01'; 
        }else{
            $this->bestBeforeDate = $date; 
        }
    }   

    public function getBestBeforeDate(){
        return $this->bestBeforeDate;
    }

    public function info()
    {
        $price =  number_format($this->price, 2, '.', ' ');
        return $this->getName().': '.$price.' € : '. $this->getWeight().'g : date limite de consommation : '.$this->getBestBeforeDate();
    }

}