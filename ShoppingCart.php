<?php 

include 'Item.php';

class ShoppingCart
{
    protected Array $items;
    protected int $totalWeight = 0;
    protected int $id;

    public function __construct()
    {
        if (!isset($GLOBALS['idShoppingCart'])) {
            $GLOBALS['idShoppingCart'] = 1 ;
            $this->id = $GLOBALS['idShoppingCart'];
        }else{
            $GLOBALS['idShoppingCart'] = $GLOBALS['idShoppingCart'] + 1;
            $this->id =  $GLOBALS['idShoppingCart'];
        }
        
    } 

    public function addItem(Item $item)
    {
        
        if (($item->getWeight() + $this->totalWeight) < 10000) {
            $this->items[] = $item ; 
            $this->totalWeight += $item->getWeight();
        }

        return false ; 
        
    }

    public function removeItem(Item $item)
    {
        if (in_array($item, $this->items)) {
            foreach ($this->items as $key => $article) {
                if ($item === $article) {
                    unset($this->items[$key]);
                }
               
                return true; 
            }
        }

        return false;
    }

    public function itemCount()
    {
        return count($this->items);
    }

    public function totalPrice()
    {
        $total = 0;
        
        foreach ($this->items as $key => $item) {
            $total += $item->getPrice();
        }

        return number_format($total, 2, ',', ' ').' €';
    }

    public function getId()
    {
        return $this->id;
    }

    public function toString()
    {
        $text = "ShoppingCart: " . $this->id . " contient " . $this->itemCount() . " éléments.Pour un prix total de " . $this->totalPrice();
        $text .= "<br> Article: <br>";
        foreach($this->items as $item){
            $text .= $item->info(). '<br>';
        }
        echo $text;
    }
}